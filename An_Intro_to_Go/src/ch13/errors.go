package main

import (
	"errors"
	"fmt"
	"reflect"
)

func main() {
	err := errors.New("error Message")
	fmt.Println(reflect.TypeOf(err))
}
