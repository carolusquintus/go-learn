package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(
		strings.Contains("test", "es"), "\n",
		strings.Count("test", "t"), "\n",
		strings.HasPrefix("test", "te"), "\n",
		strings.HasSuffix("test", "st"), "\n",
		strings.Index("test", "e"), "\n",
		strings.Join([]string{"a", "b"}, "-"), "\n",
		strings.Repeat("a", 5), "\n",
		strings.Replace("aaaa", "a", "b", 2), "\n",
		strings.Split("a-b-c-d-e", "-"), "\n",
		strings.ToLower("TEST"), "\n",
		strings.ToUpper("test"), "\n",
	)

	arr := []byte("test")
	fmt.Println(arr)

	str := string([]byte{'t', 'e', 's', 't'})
	fmt.Println(str)
}
