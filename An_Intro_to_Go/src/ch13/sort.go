package main

import (
	"fmt"
	"sort"
)

type Person struct {
	Name string
	Age  int
}

type ByName []Person

func (this ByName) Len() int {
	return len(this)
}

func (this ByName) Less(i, j int) bool {
	return this[i].Name < this[j].Name
}

func (this ByName) Swap(i, j int) {
	this[i], this[j] = this[j], this[i]
}

type ByAge []Person

func (this ByAge) Len() int {
	return len(this)
}

func (this ByAge) Less(i, j int) bool {
	return this[i].Age < this[j].Age
}

func (this ByAge) Swap(i, j int) {
	this[i], this[j] = this[j], this[i]
}

func main() {
	kids := []Person{
		{"Temeka", 3},
		{"Olga", 13},
		{"Sun", 13},
		{"Vern", 16},
		{"Delma", 5},
		{"Lucio", 8},
		{"Nestor", 9},
		{"Marissa", 1},
		{"Bernardo", 11},
		{"Breana", 8},
		{"Elsy", 2},
		{"Ellsworth", 4},
		{"Miles", 8},
	}

	sort.Sort(ByName(kids))
	fmt.Println(kids)

	sort.Sort(ByAge(kids))
	fmt.Println(kids)
}
