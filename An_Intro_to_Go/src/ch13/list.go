package main

import (
	"container/list"
	"fmt"
	"reflect"
)

func main() {
	var x list.List
	x.PushBack(1)
	x.PushBack(2)
	x.PushBack(3)

	for e := x.Front(); e != nil; e = e.Next() {
		fmt.Println(reflect.TypeOf(e))
		fmt.Println(reflect.TypeOf(e.Value))
		fmt.Println(reflect.TypeOf(e.Value.(int)))

		fmt.Println(e.Value)
		fmt.Println(e.Value.(int))
	}
}
