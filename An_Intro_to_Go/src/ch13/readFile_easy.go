package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	bs, err := ioutil.ReadFile("readFile_hard.go")

	if err != nil {
		return
	}

	str := string(bs)
	fmt.Println(str)
}
