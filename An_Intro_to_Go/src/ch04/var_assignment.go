package main

import (
	"fmt"
)

func main() {
	var x = "Hello"
	x1 := "Hello"
	x2 := "Hello"

	fmt.Println(x)
	fmt.Println(x1)
	fmt.Println(x2)

	fmt.Println(x == x1 && x == x2 && x1 == x2)
}
