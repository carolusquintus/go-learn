package main

import (
	"fmt"
)

func main() {
	var x string = "Hello World"
	fmt.Println(x)

	var y string
	y = "first"
	fmt.Println(y)
	y = "second"
	fmt.Println(y)

	var z string
	z = "first"
	fmt.Println(z)
	z += "second"
	fmt.Println(z)

	fmt.Println(x == y && x == z)
}
