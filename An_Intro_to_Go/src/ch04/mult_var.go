package main

import (
	"fmt"
)

func main() {
	var (
		a bool
		b string
		c uint16
		d rune
	)

	const (
		e = 5
		f = 10
		g = 15
	)

	d = 'a'

	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(d)

	fmt.Println(e)
	fmt.Println(f)
	fmt.Println(g)
}
