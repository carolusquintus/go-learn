package main

import (
	"ch11/math"
	"testing"
)

type testpairfloat64 struct {
	values   []float64
	expected float64
}

type testpairint struct {
	values   []int
	expected int
}

var testsAverages = []testpairfloat64{
	{[]float64{1, 2}, 1.5},
	{[]float64{1, 1, 1, 1, 1}, 1},
	{[]float64{-1, 1}, 0},
	{[]float64{}, 0},
}

var testMax = []testpairint{
	{[]int{73, 81, 60, 60}, 81},
	{[]int{32, 51, 87, 24, 3, 39}, 87},
	{[]int{7, 54, 68, 29}, 68},
	{[]int{20, 64}, 64},
}
var testMin = []testpairint{
	{[]int{26, 14, 79}, 14},
	{[]int{7, 23, 87, 77, 93, 63}, 7},
	{[]int{24}, 24},
	{[]int{45, 5, 80}, 5},
}

func TestAverage(t *testing.T) {
	for _, pair := range testsAverages {
		v := math.Average(pair.values)
		if v != pair.expected {
			t.Error(
				"\n",
				"For", pair.values, "\n",
				"expected", pair.expected, "\n",
				"got", v,
			)
		}
	}
}

func TestMin(t *testing.T) {
	for _, pair := range testMin {
		v := math.Min(pair.values...)
		if v != pair.expected {
			t.Error(
				"\n",
				"For", pair.values, "\n",
				"expected", pair.expected, "\n",
				"got", v,
			)
		}
	}
}

func TestMax(t *testing.T) {
	for _, pair := range testMax {
		v := math.Max(pair.values...)
		if v != pair.expected {
			t.Error(
				"\n",
				"For", pair.values, "\n",
				"expected", pair.expected, "\n",
				"got", v,
			)
		}
	}
}
