package main

import "fmt"

func main() {
	xs := []int{98, 93, 77, 88, 83}

	fmt.Println("Adding regular int's:", add(1, 2, 3, 4))
	fmt.Println("Adding a int slice:", add(xs...))

	x, y := f2()
	fmt.Println(x, y)
	x, y = f3()
	fmt.Println(x, y)

	fmt.Println(add(f3()))

	// Clousere or local function
	add := func(x, y int) int {
		return x + y
	}

	// Notice: the local function is called instead of besides functions add(args ...int)
	fmt.Println(add(f3()))
}

// Naming the return type
func f2() (r int, r2 int) {
	r = 1
	r2 = 2
	return
}

// Returning multiple values
func f3() (int, int) {
	return 5, 6
}

// Variadic functions or as call in java, method with varargs
func add(args ...int) int {
	total := 0
	for _, v := range args {
		total += v
	}

	return total
}
