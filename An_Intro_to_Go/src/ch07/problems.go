package main

import "fmt"

func main() {

	x := []int{
		65, 45, 32, 30, 18, 59,
		63, 87, 28, 45, 18, 58,
		81, 53, 6, 88, 75, 60,
		4, 22, 77, 24, 2, 34,
		9, 67, 36, 88, 7, 29,
		97, 82, 81, 83, 85, 25,
	}

	fmt.Println(sum(x))

	fmt.Println(add(x...))

	for _, v := range x {
		fmt.Println(half(v))
	}

	fmt.Println(greater(x...))

	nextOdd := makeOddGenerator()
	fmt.Println(nextOdd())
	fmt.Println(nextOdd())
	fmt.Println(nextOdd())

	fmt.Println(fib(0))
	fmt.Println(fib(1))
	fmt.Println(fib(2))
	fmt.Println(fib(10))
	fmt.Println(fib(100))
	fmt.Println(fib(1000))
}

func sum(nums []int) int {
	total := 0

	for _, v := range nums {
		total += v
	}

	return total
}

func add(nums ...int) int {
	total := 0

	for _, v := range nums {
		total += v
	}

	return total
}

func half(num int) (int, bool) {
	isEven := false

	if num%2 == 0 {
		isEven = true
	}

	return num / 2, isEven
}

func greater(nums ...int) int {
	max := nums[0]

	for _, v := range nums {
		if v > max {
			max = v
		}
	}

	return max
}

func makeOddGenerator() func() uint {
	i := uint(1)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}

func fib(n uint) uint {
	if n == 0 || n == 1 {
		return n
	}

	return fib(n-1) + fib(n-2)
}
