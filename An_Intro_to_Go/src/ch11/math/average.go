package math

// Finds the average of a series of numbers
func Average(xs []float64) float64 {
	total := float64(0)

	if len(xs) > 0 {
		for _, x := range xs {
			total += x
		}
		total = total / float64(len(xs))
	}

	return total
}

// Finds the maximum number in a slice
func Max(nums ...int) int {
	max := nums[0]

	for _, v := range nums {
		if v > max {
			max = v
		}
	}

	return max
}

// Finds the minimum number in a slice
func Min(nums ...int) int {
	min := nums[0]

	for _, v := range nums {
		if v < min {
			min = v
		}
	}

	return min
}
