package main

import (
	c11m "ch11/math" // Using alias for math.go package
	"ch11/other"
	"fmt"
	"math"
)

func main() {
	xs := []float64{1, 2, 3, 4}

	x := []int{
		65, 45, 32, 30, 18, 59,
		63, 87, 28, 45, 18, 58,
		81, 53, 6, 88, 75, 60,
		4, 22, 77, 24, 2, 34,
		9, 67, 36, 88, 7, 29,
		97, 82, 81, 83, 85, 25,
	}

	avg := c11m.Average(xs)

	fmt.Println(avg)
	fmt.Println(math.Pi)

	fmt.Println("Max", c11m.Max(x...))
	fmt.Println("Min", c11m.Min(x...))

	other.FizzBuzz()
}
