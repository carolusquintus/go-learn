package other

import "fmt"

func FizzBuzz() { // A function must begin with capital letter to be seen
	for i := 1; i < 20; i++ {
		word := ""

		if i%3 == 0 {
			word += "Fizz"
		}

		if i%5 == 0 {
			word += "Buzz"
		}

		if word != "" {
			fmt.Println(i, ":", word)
		}
	}
}
