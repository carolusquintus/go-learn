package main

import "fmt"

func main() {
	x := 5
	zero(&x)
	fmt.Println("x:", x)
	fmt.Println("&x:", &x)
	//fmt.Println("*x", *x)

	xPtr := new(int)
	one(xPtr)
	fmt.Println("xPtr:", xPtr)
	fmt.Println("&xPtr:", &xPtr)
	fmt.Println("*xPtr:", *xPtr)

	y := 1.5
	square(&y)
	fmt.Println(y)

	x1 := 1
	y1 := 2
	swap(&x1, &y1)
	fmt.Println(x1, y1)
}

func zero(xPtr *int) {
	*xPtr = 0
}

func one(xPtr *int) {
	*xPtr = 1
}

func square(y *float64) {
	*y = (*y) * (*y)
}

func swap(x *int, y *int) {
	tmp := *y
	*y = *x
	*x = tmp
}
