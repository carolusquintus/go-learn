package main

import "fmt"

func main() {
	OneToTen1()
	OneToTen2()
	OneToTen3()
	OneToTen4()
}

func OneToTen1() {
	fmt.Println(1)
	fmt.Println(2)
	fmt.Println(3)
	fmt.Println(4)
	fmt.Println(5)
	fmt.Println(6)
	fmt.Println(7)
	fmt.Println(8)
	fmt.Println(9)
	fmt.Println(10)
}

func OneToTen2() {
	fmt.Println(`1
2
3
4
5
6
7
8
9
10`)
}

func OneToTen3() {
	// This works like a while
	i := 1
	for i <= 10 {
		fmt.Println(i)
		i += 1
	}
}

func OneToTen4() {
	for i := 1; i <= 10; i++ {
		fmt.Println(i)
	}
}
