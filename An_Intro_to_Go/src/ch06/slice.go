package main

import "fmt"

func main() {

	c := make([]int, 10, 10)

	c[0] = 40
	c[1] = 45
	c[2] = 50
	c[3] = 55
	c[4] = 60
	c[5] = 65
	c[6] = 70
	c[7] = 75
	c[8] = 80
	c[9] = 80

	fmt.Println("Original array c[]:\t", c)

	fmt.Println("c[] Length:\t", len(c))
	fmt.Println("c[] Capacity:\t", cap(c))

	for _, val := range c {
		if val%3 == 0 && val%5 == 0 {
			fmt.Println(val)
		}
	}

	fmt.Println("c[2:5] = From index 2 to index 5 - 1 :\t", c[2:5])

	c1 := append(c, 95, 100, 110)

	fmt.Println("c[]:\t", c)
	fmt.Println("c1[]:\t", c1)

	c2 := append(c1, 115, 120, 125)
	c3 := append(c, 130, 135, 140)
	c = append(c, 130, 135, 140)

	fmt.Println("c[]:\t", c)
	fmt.Println("c2[]:\t", c2)
	fmt.Println("c3[]:\t", c3)

	c4 := make([]int, 4)
	copy(c4, c3[2:11])

	fmt.Println("c3[]:\t", c3)
	fmt.Println("c4[]:\t", c4)
}
