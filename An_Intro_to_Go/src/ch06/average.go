package main

import "fmt"

func main() {
	var x [5]float64

	x[0] = 98
	x[1] = 93
	x[2] = 77
	x[3] = 82
	x[4] = 83

	// Short syntax to write arrays
	x2 := [5]float64{
		99, 94,
		78, 83,
		84,
	}

	//var total float64 = 0.0
	//var total2 float64 = 0.0

	total := 0.0
	total2 := 0.0

	// Classic example trough array
	for i := 0; i < len(x); i++ {
		total += x[i]
	}

	// For-each like example trough arry
	for , value := range x2 {
		total2 += value
	}

	fmt.Println(total / float64(len(x)))
	fmt.Println(total2 / float64(len(x2)))
}
