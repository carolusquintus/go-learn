package main

import "fmt"
import "sort"

func main() {
	x := make(map[string]int)
	x["k"] = 0
	x["e"] = 1
	x["y"] = 2

	_, containsZ := x["z"]

	fmt.Println("x :", x)
	fmt.Println("x[\"k\"] :", x["k"])

	if !containsZ {
		x["z"] = x["e"] * x["y"]
		fmt.Println("x[\"z\"] :", x["z"])
	}

	fmt.Println(x)

	delete(x, "y")

	fmt.Println(x)

	elements := make(map[string]string)

	elements["H"] = "Hydrogen"
	elements["He"] = "Helium"
	elements["Li"] = "Lithium"
	elements["Be"] = "Beryllium"
	elements["B"] = "Boron"
	elements["C"] = "Carbon"
	elements["N"] = "Nitrogen"
	elements["O"] = "Oxygen"
	elements["F"] = "Fluorine"
	elements["Ne"] = "Neon"

	elements2 := map[string]string{
		"H":  "Hydrogen",
		"He": "Helium",
		"Li": "Lithium",
		"Be": "Beryllium",
		"B":  "Boron",
		"C":  "Carbon",
		"N":  "Nitrogen",
		"O":  "Oxygen",
		"F":  "Fluorine",
		"Ne": "Neon",
	}

	elements3 := map[string]map[string]string{
		"H": map[string]string{
			"name":  "Hydrogen",
			"state": "gas",
		},
		"He": map[string]string{
			"name":  "Helium",
			"state": "gas",
		},
		"Li": map[string]string{
			"name":  "Lithium",
			"state": "solid",
		},
		"Be": map[string]string{
			"name":  "Beryllium",
			"state": "solid",
		},
		"B": map[string]string{
			"name":  "Boron",
			"state": "solid",
		},
		"C": map[string]string{
			"name":  "Carbon",
			"state": "solid",
		},
		"N": map[string]string{
			"name":  "Nitrogen",
			"state": "gas",
		},
		"O": map[string]string{
			"name":  "Oxygen",
			"state": "gas",
		},
		"F": map[string]string{
			"name":  "Fluorine",
			"state": "gas",
		},
		"Ne": map[string]string{
			"name":  "Neon",
			"state": "gas",
		},
	}

	fmt.Println("elements[\"Li\"] :", elements["Li"])
	fmt.Println("elements[\"Un\"] :", elements["Un"])
	fmt.Println("elements2[\"Li\"] :", elements2["Li"])
	fmt.Println("elements2[\"Un\"] :", elements2["Un"])
	fmt.Println("elements3[\"Li\"] :", elements3["Li"])
	fmt.Println("elements3[\"Un\"] :", elements3["Un"])

	symbolsOrdered := make([]string, 0)

	for k, _ := range elements3 {
		symbolsOrdered = append(symbolsOrdered, k)
	}

	sort.Strings(symbolsOrdered)

	symbolsOrdered = append(symbolsOrdered, "Un")

	for _, k := range symbolsOrdered {
		_, exist := elements3[k]
		_, exist2 := elements2[k]

		if exist && exist2 {
			fmt.Println(k, elements3[k])
			fmt.Println(k, elements2[k])
		} else {
			fmt.Println(k, "doesn't exists")
		}
	}

	if name, exist := elements["B"]; exist {
		fmt.Println(name, exist)
	}

	v := make([]int, 3, 9)

	fmt.Println(len(v))

}
