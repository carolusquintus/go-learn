package main

import (
	"fmt"
	"math"
)

type MultiShape struct {
	shapes []Shape
}
type Shape interface {
	area() float64
	perimeter() float64
}

type Circle struct {
	x, y, r float64
}

type Rectangle struct {
	x1, y1, x2, y2 float64
}

func main() {
	var rx1, ry1 float64 = 1, 1
	var rx2, ry2 float64 = 11, 11

	r := Rectangle{0, 0, 10, 10}
	r1 := new(Rectangle)
	r1.x1 = 1
	r1.x2 = 11
	r1.y1 = 1
	r1.y2 = 11

	fmt.Println("r.area", "=", r.area())
	fmt.Println("r.perimeter", "=", r.perimeter())
	fmt.Println("r1.area", "=", r1.area())
	fmt.Println("r1.perimeter", "=", r1.perimeter())
	fmt.Println("rectangleArea", "=", rectangleArea(rx1, ry1, rx2, ry2))

	c := Circle{0, 0, 5}
	c1 := new(Circle)
	c1.x = 1
	c1.y = 1
	c1.r = 6

	fmt.Println("circleArea", "=", circleArea(c))
	fmt.Println("circleAreaPointer", "=", circleAreaPointer(&c))

	fmt.Println("circleArea", "=", circleArea(*c1))
	fmt.Println("circleAreaPointer", "=", circleAreaPointer(c1))

	fmt.Println("c.area", "=", c.area())
	fmt.Println("c.perimeter", "=", c.perimeter())
	fmt.Println("c1.area", "=", c1.area())
	fmt.Println("c1.perimeter", "=", c1.perimeter())

	measure(c)
	measure(c1)
	measure(r)
	measure(r1)

	fmt.Println("Total area:", totalArea(c, c1, r, r1))

	m := MultiShape{[]Shape{c, c1, r, r1}}
	fmt.Println("Multishape area: ", m.area())
}

func distance(x1, y1, x2, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1

	return math.Sqrt(a*a + b*b)
}

func rectangleArea(x1, y1, x2, y2 float64) float64 {
	l := distance(x1, y1, x1, y2)
	w := distance(x1, y1, x2, y1)

	return l * w
}

func circleArea(c Circle) float64 {
	return math.Pi * (c.r * c.r)
}

func circleAreaPointer(c *Circle) float64 {
	return math.Pi * (c.r * c.r)
}

/*func (c *Circle) area() float64 {
	return math.Pi * (c.r * c.r)
}

func (r *Rectangle) area() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)

	return l * w
}*/

// To implement Interface methods the receiver musn't be a pointer
func (c Circle) area() float64 {
	return math.Pi * (c.r * c.r)
}

func (c Circle) perimeter() float64 {
	return math.Pi * (c.r + c.r)
}

func (r Rectangle) area() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)

	return l * w
}

func (r Rectangle) perimeter() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)

	return (2 * l) + (2 * w)
}

func (m MultiShape) area() float64 {
	var area float64
	for _, s := range m.shapes {
		area += s.area()
	}

	return area
}
func measure(s Shape) {
	fmt.Println(s.area())
}

func totalArea(shapes ...Shape) float64 {
	var total float64

	for _, s := range shapes {
		total += s.area()
	}

	return total
}
